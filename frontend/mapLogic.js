let lookupMap;
const map = L.map('map').setView([51.505, -0.09], 3);
let geojson;
L.tileLayer('https:tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    noWrap: true
}).addTo(map);

const myGeoJSONPath = 'custom.geo.json';

function getColor(d) {
    const f = lookupMap.get(d);
    if (f === undefined)
        return undefined
    return      f > 30 ? '#bd0026' :
        f > 25   ? '#f03b20' :
            f > 20  ? '#fd8d3c' :
                f > 15  ? '#fecc5c' :
                    f > 10  ? '#ffffcc' :
                        f > 5   ? '#a1dab4' :
                            f > 0   ? '#41b6c4' :
                                f > -5   ? '#2c7fb8' :
                                    '#000'
}
function style(feature) {
    return {
        fillColor: getColor(feature.properties.iso_a2_eh),
        opacity: 1,
        weight: 0.7,
        color: 'black',
        fillOpacity: 0.5
    };
}

function highlightFeature(e) {
    const layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#135'
    });

    layer.bringToFront();
}
function resetHighlight(e) {
    geojson.resetStyle(e.target);
}

var lastClicked;
function populatePage(e) {

    /*if(prevCountry !== lastClicked) {
        prevCountry = lastClicked;
        populateStations(lastClicked)
    }*/

    if(lastClicked === e) {
        document.getElementById("info").remove();
        lastClicked = null;
        return
    }
    lastClicked = e;
    if (document.getElementById("info") == null) {
        let div = document.createElement("div")
        let tdiv = document.createElement("table-div")
        let frame = document.createElement("iframe")
        let table = document.createElement("table")
        table.id = "table"
        div.id = "info";
        frame.id = "frame";
        tdiv.append(table)
        tdiv.style = "flex:1;margin:15px;overflow: auto"
        frame.style = "height:100%;flex:2;margin:15px"
        div.style = "margin:auto; width: 100%;height: 900px; display:flex;"
        div.append(tdiv, frame)
        document.getElementById("body").append(div)
        countryAjax(e, frame, table)
    }else {
        countryAjax(e, document.getElementById("frame"), document.getElementById("table"))
    }

}

let prevCountry

function zoomToFeature(e) {
    populatePage(e.target.feature.properties.iso_a2_eh)
    map.fitBounds(e.target.getBounds());

    map.eachLayer(function (layer) {
        if (layer instanceof L.Circle) {
            map.removeLayer(layer);
        }
    });


}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}


let load = false

if (!load) {
    fetch('http://127.0.0.1:8000/daily_world', {
        method: "GET", headers: {
            'Access-Control-Allow-Origin': '*',
        }
    })
        .then((response) => response.json())
        .then((json) => {
            let myMap = new Map();

            json.forEach((k) => {
                myMap.set(k.country, k.tavg)
            })

            lookupMap = myMap;
        }).then(() => {
            $.getJSON(myGeoJSONPath, function (data) {
                geojson = L.geoJson(data, {
                    clickable: true,
                    style: style,
                    onEachFeature: onEachFeature
                }).addTo(map);
            })

            document.getElementById("loader").style.display = "none";
        }
    )
    load = true;
}