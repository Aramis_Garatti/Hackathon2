function countryAjax(code, frame, table) {
    document.getElementById("loader").style.display = "inline";
    fetch("http://127.0.0.1:8000/daily_by_country?country=" + code, {
        method: "GET", headers: {
            'Access-Control-Allow-Origin': '*',
        }
    })
    .then(function (response) {
        //frame.src = response.url;
        return response.json();
    })
    .then(function (json) {
        frame.src = "http://127.0.0.1:8000/graph"
        $("#table *").remove();

        console.log(map.layers)

        let thead = document.createElement("thead")
        let tbody = document.createElement("tbody")
        let tr = document.createElement("tr")
        let e1 = document.createElement("th");
        e1.textContent = "Station Name"
        tr.append(e1)

        let e3 = document.createElement("th")
        e3.textContent = "Max Temp"
        tr.append(e3)

        let e4 = document.createElement("th")
        e4.textContent = "Min Temp"
        tr.append(e4)

        let e5 = document.createElement("th")
        e5.textContent = "Avg Temp"
        tr.append(e5)
        thead.append(tr)

        table.append(thead)

        for(let i = 0; i < json.length; i++) {
            L.circle([json[i].latitude, json[i].longitude], {
                color: 'red',
                fillColor: '#ff666',
                fillOpacity: 0.5,
                radius: 500,
            }).addTo(map);

            let row = document.createElement("tr");
            let e1 = document.createElement("th");
            e1.textContent = json[i].name;
            row.append(e1)

            let e3 = document.createElement("th")
            e3.textContent = json[i].tmax;
            row.append(e3)

            let e4 = document.createElement("th")
            e4.textContent = json[i].tmin;
            row.append(e4)

            let e5 = document.createElement("th")
            e5.textContent = json[i].tavg;
            row.append(e5)

            tbody.append(row)
        }
        table.append(tbody)

        document.getElementById("loader").style.display = "none";
    })
}