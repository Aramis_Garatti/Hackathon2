from flask import Flask, render_template, request
from flask_cors import CORS
from analysis.daily_dataset.daily import df_creator_today
from analysis.monthly_dataset.monthly import df_creator_by_country
from analysis.daily_world.daily_world import df_daily_world_creator
from meteostat import Stations

from analysis.create_nation.create import get_html
from os.path import exists
import pandas as pd
from datetime import date
from pathlib import Path

app = Flask(__name__)
CORS(app)

stations = Stations()
station = stations.fetch()


@app.route('/worldhistline')
def worldhistline():
    return render_template('choroplethkaggle.html')

@app.route('/worldhistgraph')
def worldhistgraph():
    return render_template('linekaggle.html')

@app.route('/graph')
def prova1():
    return render_template('graph.html')


@app.route('/daily_by_country')
def daily_by_country():
    country = request.args.get("country")
    get_html(country)
    cwd = str(Path().absolute()) + f"/backend/analysis/dataset/daily-{country}-{date.today()}.json"
    if exists(cwd):
       # df = pd.read_json(f"/backend/analysis/dataset/{country}.json", orient='records')
        return open(cwd)
    else:
        df = df_creator_today(country, station)
        df = df.groupby("name").mean()
        df = df.reset_index()
        df.to_json(cwd, orient='records')
        return df.to_json(orient="records")


@app.route('/monthly_by_country')
def monthly_by_country():
    country = request.args.get("country")
    cwd = str(Path().absolute()) + f"/backend/analysis/dataset/monthly-{country}-{date.today()}.json"
    if exists(cwd):
        df = pd.read_json(cwd)
        return df.to_json(orient="records")
    else:
        df = df_creator_by_country(country, station)
        df = df.reset_index()
        df.to_json(cwd)
        return df.to_json(orient="records")
cwd = str(Path().absolute()) + f"/backend/analysis/dataset/daily-world-{date.today()}.json"
if exists(cwd):
    df = pd.read_json(cwd)
else:
    df = df_daily_world_creator(stations, station)
    df.to_json(cwd)


@app.route('/daily_world')
def daily_world():
    return df.to_json(orient="records")


if __name__ == '__main__':
    app.run(port=8000, debug=True)
