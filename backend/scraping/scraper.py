from meteostat import Stations, Monthly
from datetime import datetime
import pandas as pd
import country_converter as coco

def scrape():
    f = pd.DataFrame()
    f.to_csv(f'backend\\analysis\\dataset\\years\\test.csv')
    cc = coco.CountryConverter()
    ct = cc.ISO2['ISO2']
    stations = Stations()
    allstations = []
    for i in ct:
        station = stations.region(country=str(i))
        start = datetime(2000, 1, 1)
        end = datetime(2020, 12, 31)
        station = station.inventory('monthly', (start, end))
        data = station.fetch(20)
        allstations.append(data)


    df = pd.DataFrame(allstations[0])
    n = 0
    for i in allstations:
        if n == 0:
            n = 1
            continue
        df = pd.concat([df, pd.DataFrame(i)])

    years_to_get=[]
    for i in range(2010, 2021):
        years_to_get.append(i)

    for i in years_to_get:
        start = datetime(i, 1, 1)
        end = datetime(i, 12, 31)
        data = Monthly(loc=df, start=start, end=end)
        df_ms = data.fetch()
        df_ms = pd.merge(df_ms.reset_index(), df.reset_index(), left_on='station', right_on='id', how='left').drop('id', axis=1)
        df_ms.drop(columns=['name', 'hourly_start', 'hourly_end', 'daily_start', 'daily_end', 'monthly_start', 'monthly_end'], inplace=True)
        df_ms.to_csv(f'backend\\analysis\\dataset\\years\\{i}.csv')


if __name__ == "__main__":
    scrape()