import pandas as pd
import plotly.express as px
import country_converter as coco
from pathlib import Path

def get_html(nation):
    cc = coco.CountryConverter()
    nation = cc.convert(names=nation, to='ISO3')
    df = pd.read_csv(str(Path().absolute()) + f"/backend/analysis/dataset/meteostat.csv")
    x = df.loc[df['ISO3']==nation]
    i = x.groupby('Year').mean().reset_index().sort_values('Year')
    fig = px.line(i, x='Year', y='tavg')
    fig.update_layout(title=f'Average temperature over the years in {cc.convert(names=f"{nation}", to="short_name")}',
                      yaxis_title="Average temperature °C")
    fig.write_html(str(Path().absolute()) + f"/backend/templates/graph.html")