def df_daily_world_creator(stations, station_c):
    from meteostat import Daily
    from datetime import datetime, date
    import pandas as pd

    start = datetime.strftime(date.today(), "%d-%m-%Y %H:%M:%S")
    end = datetime.strftime(date.today(), "%d-%m-%Y %H:%M:%S")

    df_sing = pd.DataFrame()

    for i in station_c.country.unique():
        temp = stations.region(country=str(i))
        temp = temp.fetch(1)
        df_sing = pd.concat([df_sing, temp], axis=0)

    df_sing = df_sing[df_sing.columns[:-7]]
    df_sing = df_sing.reset_index()

    df_finale = pd.DataFrame()

    for idx, row in df_sing.iterrows():
        data = Daily(row.id, start, end)
        data = data.fetch()
        data["country"] = row.country
        df_finale = pd.concat([df_finale, data], axis=0)

    df_finale = df_finale[["country", "tavg"]]
    df_finale = df_finale.groupby("country").mean()
    return df_finale.reset_index()
