def df_creator_by_country(country, station):
    from meteostat import Monthly
    from datetime import datetime
    import pandas as pd

    station = station.reset_index()

    df = station[station.country == country]
    df = df.reset_index()
    start = datetime(2013, 1, 1)
    end = datetime(2023, 1, 1)

    df_finale = pd.DataFrame()

    for i in df.id.unique():
        data = Monthly(i, start, end)
        data = data.fetch()
        df_finale = pd.concat([df_finale, data], axis=0)

    return df_finale.groupby("time").mean()
