import pandas as pd
import plotly.express as px
import country_converter as coco
 
def get_html(nation):
    cc = coco.CountryConverter()
    nation = cc.convert(names=nation, to='ISO3')
    df = pd.read_csv('backend\\analysis\\dataset\\meteostat.csv')
    x = df.loc[df['ISO3']==nation]
    i = x.groupby('Year').mean().reset_index().sort_values('Year')
    fig = px.line(i, x='Year', y='tavg')
    fig.update_layout(title=f'Average temperature over the years in {cc.convert(names=f"{nation}", to="short_name")}',
                      yaxis_title="Average temperature °C")
    return fig
    
if __name__ == "__main__":
    x = get_html('Italy')
    x.show()
    x.write_html("backend/templates/graph.html")
    
    
    