def df_creator_today(country, station):
    from meteostat import Daily
    from datetime import date
    from datetime import datetime
    import pandas as pd

    start = datetime.strftime(date.today(), "%d-%m-%Y %H:%M:%S")
    end = datetime.strftime(date.today(), "%d-%m-%Y %H:%M:%S")

    station = station.reset_index()

    df = station[station.country == country]
    df = df.reset_index()
    df = df[["id", "name", "latitude", "longitude"]]

    df_finale = pd.DataFrame()

    for i in df.id.unique():
        data = Daily(i, start, end)
        data = data.fetch()
        data["id"] = i
        df_finale = pd.concat([df_finale, data])

    df_finito = pd.merge(df, df_finale, on="id")
    return df_finito[df_finito.columns[:-7]]
